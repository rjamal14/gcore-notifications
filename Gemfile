source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.5'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.0.3', '>= 6.0.3.2'
# Use Puma as the app server
gem 'puma', '~> 4.1'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.7'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 4.0'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Active Storage variant
# gem 'image_processing', '~> 1.2'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.2', require: false

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
# gem 'rack-cors'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
end

group :development do
  gem 'listen', '~> 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  # Setup capistrano deployment
  gem 'capistrano'
  gem 'capistrano-bundler', '~> 1.4'
  gem 'capistrano-rails', '~> 1.4'
  gem 'capistrano-rbenv', '~> 2.1.4'
  gem 'capistrano3-delayed-job', '~> 1.0'
  gem 'capistrano3-puma'

  # Rubocop for Code Styling
  gem 'rubocop-rails', require: false
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]

# Add by us
gem 'active_model_serializers', '~> 0.10.0' # Custom render json
gem 'bson_ext', '~> 1.5.1' # Accelerate BSON serializer
gem 'bunny', '>= 2.14.1' # RabbitMQ Client
gem 'dotenv-rails', '2.7.5' # Environtment Variables
gem 'enumerize', '~> 2.3.1' # Enumerated attributes
gem 'jwt', '2.2.1' # json web token
gem 'kaminari-mongoid', '1.0.1' # Pagination
gem 'mongoid', '~> 7.0.5' # Mongodb
gem 'rack-cors', '1.1.1' # Handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rails-i18n', '~> 6.0.0' # Translations
gem 'sneakers', '~> 2.11.0' # Background Processing for RabbitMQ
